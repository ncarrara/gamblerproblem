/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import model.Model;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class JFrameGambleur extends JFrame implements Observer {

    public static void main(String[] args) {
        new JFrameGambleur();
    }

    JLabel aimv;
    JLabel pv;
    JLabel tv;
    JLabel gv;
    Model model;

    public JFrameGambleur() {
        super();
        model = new Model(/*64, 0.6*/);
        model.addObserver(this);
        JPanelGambleur xyz = new JPanelGambleur(model, "title", "legende");
        add(xyz, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel south = new JPanel(/*new GridLayout(2,3)*/);

        JLabel aiml = new JLabel("aim");
        south.add(aiml);
        aimv = new JLabel(model.getAim() + "");
        JSlider echelle = new JSlider(JSlider.HORIZONTAL, 0, 100, 100);
        echelle.setValue((int) (model.getAim()));
        south.add(echelle);
        south.add(aimv);
        echelle.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    model.setAim(source.getValue());

                }
            }
        });

        JLabel pl = new JLabel("p");
        south.add(pl);
        pv = new JLabel(model.getP() + "");
        JSlider echellep = new JSlider(JSlider.HORIZONTAL, 0, 100, 100);
        echellep.setValue((int) (model.getP() * 100.));

        south.add(echellep);
        south.add(pv);
        echellep.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    model.setP(source.getValue() / 100.);

                }
            }
        });

        JLabel tl = new JLabel("theta");
        south.add(tl);
        tv = new JLabel(model.getTheta() + "");
        JSlider echellet = new JSlider(JSlider.HORIZONTAL, 1, 100000000, 10000);
        echellet.setValue((int) (model.getP() * 1000000));
        south.add(echellet);
        south.add(tv);
        echellet.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    model.setTheta(source.getValue() / 1000000.);

                }
            }
        });
        JLabel gl = new JLabel("gamma");
        south.add(gl);
        gv = new JLabel(model.getGamma() + "");
        JSlider echelleg = new JSlider(JSlider.HORIZONTAL, 0, 10000, (int) (model.getGamma()* 10000.));
//        echelleg.setValue((int) (model.getGamma()* 10000.));
        south.add(echelleg);
        south.add(gv);
        echelleg.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    model.setGamma(source.getValue() / 10000.);

                }
            }
        });

        add(south, BorderLayout.SOUTH);
        pack();
        setTitle("swing");
        setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        aimv.setText(model.getAim() + "");
        pv.setText(model.getP() + "");
        tv.setText(model.getTheta() + "");
        gv.setText(model.getGamma() + "");
    }
}
