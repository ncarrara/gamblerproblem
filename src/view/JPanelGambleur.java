/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import mdp.Politique;
import model.Model;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class JPanelGambleur extends JPanel implements Observer {

    private Model model;

    protected String titre;
    protected String legende;
    protected ChartPanel _chartPanel;
    private JFreeChart evaluationjfreechart;
    private JFreeChart actionjfreechart;
    private ChartPanel chartpanel;
    private ChartPanel actionpanel;
    private JCheckBox iteration;
    private JCheckBox evaluation;

    public JPanelGambleur(Model model,
            String titre,
            String legende) {

        super(new GridLayout(1, 2));
        model.addObserver(this);
        this.titre = titre;
        this.legende = legende;
        this.model = model;
        jpeval = new JPanel(new BorderLayout());
        ItemListener listener = new ItemListener() {
            public void itemStateChanged(ItemEvent event) {
                renderEvaluationGraph();
                renderActionGraph();
            }
        };

        iteration = new JCheckBox("V from algo value iteration");
        iteration.setSelected(true);
        iteration.addItemListener(listener);
        evaluation = new JCheckBox("V from algo policy evaluation");
        evaluation.setSelected(true);
        evaluation.addItemListener(listener);

        JPanel north = new JPanel(new GridLayout(1, 4));
        north.add(iteration);
        north.add(evaluation);

        renderEvaluationGraph();
        renderActionGraph();

        jpeval.add(chartpanel, BorderLayout.CENTER);
        jpeval.add(north, BorderLayout.NORTH);
        add(jpeval);
        add(actionpanel);

    }

    private JPanel jpeval;

    /**
     * Met a jour le graph des resultats
     */
    protected final void renderEvaluationGraph() {
        if (chartpanel != null) {
            jpeval.remove(chartpanel);
        }
        revalidate();
        XYSeries seriesEvaluation = getEvaluationData();
        XYSeries seriesIteration = geValueIterationData();
//        XYSeries seriesAction = getActionData();
//        XYSeries seriesPredictLearn = getPredictLearningData();
//        XYSeries seriesPredictTest = getPredictTestData();
//        XYSeries seriesTest = getTestData();
        XYSeriesCollection datasetEvaluation = new XYSeriesCollection();

//        dataset.addSeries(seriesTest);
//        dataset.addSeries(seriesPredictLearn);
//        dataset.addSeries(seriesPredictTest);
        if (iteration.isSelected()) {
            datasetEvaluation.addSeries(seriesIteration);

        }
        if (evaluation.isSelected()) {
            datasetEvaluation.addSeries(seriesEvaluation);
        }

        evaluationjfreechart = ChartFactory.createScatterPlot(titre, "state", "evalutation with best estimate policy", datasetEvaluation, PlotOrientation.VERTICAL, false, true, true);
        evaluationjfreechart.setBackgroundPaint(Color.white);
        XYPlot xyplot = (XYPlot) evaluationjfreechart.getPlot();
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);

        xyplot.setAxisOffset(new RectangleInsets(4D, 4D, 4D, 4D));
        xyplot.setRangeTickBandPaint(new Color(200, 200, 100, 100));
        XYDotRenderer xydotrenderer = new XYDotRenderer();
        if (datasetEvaluation.getSeriesIndex("evaluation") >= 0) {
            xydotrenderer.setSeriesPaint(datasetEvaluation.getSeriesIndex("evaluation"), Color.GREEN);
        }
        if (datasetEvaluation.getSeriesIndex("iteration") >= 0) {
            xydotrenderer.setSeriesPaint(datasetEvaluation.getSeriesIndex("iteration"), Color.BLUE);
        }
        xydotrenderer.setDotWidth(4);
        xydotrenderer.setDotHeight(4);
        xyplot.setRenderer(xydotrenderer);
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
        chartpanel = new ChartPanel(evaluationjfreechart);

        jpeval.add(chartpanel);
        this.repaint();

    }

    protected final void renderActionGraph() {
        if (actionpanel != null) {
            remove(actionpanel);
        }
        revalidate();
        XYSeries seriesAction = getActionData();
//        XYSeries seriesPredictLearn = getPredictLearningData();
//        XYSeries seriesPredictTest = getPredictTestData();
//        XYSeries seriesTest = getTestData();
        XYSeriesCollection datasetAction = new XYSeriesCollection();
        datasetAction.addSeries(seriesAction);
//        dataset.addSeries(seriesTest);
//        dataset.addSeries(seriesPredictLearn);
//        dataset.addSeries(seriesPredictTest);
        actionjfreechart = ChartFactory.createScatterPlot(titre, "state", "action from best policy", datasetAction, PlotOrientation.VERTICAL, false, true, true);
        actionjfreechart.setBackgroundPaint(Color.white);
        XYPlot xyplot = (XYPlot) actionjfreechart.getPlot();
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);

        xyplot.setAxisOffset(new RectangleInsets(4D, 4D, 4D, 4D));
        xyplot.setRangeTickBandPaint(new Color(200, 200, 100, 100));
        XYDotRenderer xydotrenderer = new XYDotRenderer();
        xydotrenderer.setDotWidth(4);
        xydotrenderer.setDotHeight(4);
        xyplot.setRenderer(xydotrenderer);
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
        actionpanel = new ChartPanel(actionjfreechart);

        add(actionpanel);
        this.repaint();

    }

    /**
     * Cree les donnee de la courbe
     *
     * @return le tableau de donnees de la courbe
     */
    protected XYSeries getEvaluationData() {
        XYSeries series = new XYSeries("evaluation", true);
//        for (Example<Double, Double> e : model.getBase_learning()) {
//            series.add(e.getX(), e.getY());
//        }
        double[] values = model.getCurrent_V_from_evaluation();
        for (int i = 0; i < values.length; i++) {
            series.add(i, values[i]);
        }
        return series;

    }

    protected XYSeries geValueIterationData() {
        XYSeries series = new XYSeries("iteration", true);
//        for (Example<Double, Double> e : model.getBase_learning()) {
//            series.add(e.getX(), e.getY());
//        }
        double[] values = model.getCurrent_V_from_value_iteration();
        for (int i = 0; i < values.length; i++) {
            series.add(i, values[i]);
        }
        return series;

    }

    protected XYSeries getActionData() {
        XYSeries series = new XYSeries("prediction learn", true);
        Politique values = model.getCurrent_pi();
        for (int i = 0; i < values.size(); i++) {
            if (values.getActionProba(i).size() == 1) {
                series.add(i, values.getActionProba(i).get(0).getAction().id);
            }
        }
        return series;

    }

//    protected XYSeries getTestData() {
//        XYSeries series = new XYSeries("test", true);
////        for (Example<Double, Double> e : model.getBase_test()) {
////            series.add(e.getX(), e.getY());
////        }
////        System.out.println("series : "+Arrays.deepToString(series.toArray()));
//        return series;
//
//    }
//
//    protected XYSeries getPredictTestData() {
//        XYSeries series = new XYSeries("prediction test", true);
////        for (Example<Double, Double> e : model.getBase_test()) {
////            series.add((double) e.getX(), (double) model.getP().predict(e.getX()));
////        }
////        System.out.println("series : "+Arrays.deepToString(series.toArray()));
//        return series;
//
//    }
    @Override
    public void update(Observable o, Object arg) {
//        System.out.println("ok");
        renderEvaluationGraph();
        renderActionGraph();
        chartpanel.repaint();
        actionpanel.repaint();
    }

}
