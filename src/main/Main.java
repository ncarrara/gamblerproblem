/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.Arrays;
import mdp.Action;
import static mdp.MarkovDecisionProcessFast.deterministicActionToPolitique;
import mdp.Politique;
import model.Model;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class Main {

    public static void main(String[] args) {
        Model m = new Model(/*64, 0.1*/);

        Politique p = m.getMdp().valueIteration(0.00001, 0.99/*,100*/);

        double[] vbis = m.getMdp().policyEvalutation(p, 0.00001, 0.99);
        System.out.println("vbis : " + Arrays.toString(vbis));
    }
}
