/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import mdp.Action;
import mdp.Direction;
import mdp.MarkovDecisionProcessFast;
import mdp.Politique;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class Model extends Observable {

    private MarkovDecisionProcessFast mdp;

    private int aim = 64;

    private double p = 0.6;

    private double gamma = 0.99;
    private double theta = 0.0001;

    public Model() {
        constructMDP();
        computeValue();
    }

    public void reset() {
        constructMDP();
        computeValue();
        update();
    }

    private void constructMDP() {
        List<Action>[] states = new List[aim + 1];

        states[0] = new ArrayList<>();

        states[aim] = new ArrayList<>();

        for (int s = 1; s <= aim - 1; s++) {
            List<Action> actions = new ArrayList<>();
            for (int a = 1; a <= s; a++) {
                if (s + a <= aim) {
                    List<Direction> directions = new ArrayList<>();
                    directions.add(new Direction(s - a, 1 - p, 0));
                    directions.add(new Direction(s + a, p, s + a == aim ? 100 : 0));
                    actions.add(new Action(directions, a));
                }
            }
            states[s] = actions;
        }
//        System.out.println("--------- actions possibles ---------");
//        for (int i = 0; i < states.length; i++) {
//            System.out.println("etat "+i+" : " + states[i]);
//        }
//        System.out.println("--------------------------------------");
        mdp = new MarkovDecisionProcessFast(states);
//        System.out.println("okay");

    }

    private Politique current_pi;

    private double[] current_V_from_evaluation;

    private double[] current_V_from_value_iteration;

    private void computeValue() {
//        System.out.println("-------------------- valueIteration ---------------------");
        current_pi = getMdp().valueIteration(theta, gamma);
//        System.out.println("Politique : "+current_pi);
//        System.out.println("--------------------------------------------------------");
        current_V_from_value_iteration = getMdp().getV_from_value_iteration();
        // veirification
//        System.out.println("policyEvalutation ...");
        current_V_from_evaluation = getMdp().policyEvalutation(current_pi, theta, gamma);
//        System.out.println("done");
    }

    /**
     * @return the mdp
     */
    public MarkovDecisionProcessFast getMdp() {
        return mdp;
    }

    /**
     * @param mdp the mdp to set
     */
    public void setMdp(MarkovDecisionProcessFast mdp) {
        this.mdp = mdp;
    }

    public void update() {
        setChanged();
        notifyObservers();
    }

    /**
     * @return the aim
     */
    public int getAim() {
        return aim;
    }

    /**
     * @param aim the aim to set
     */
    public void setAim(int aim) {
        this.aim = aim;
        reset();
    }

    /**
     * @return the p
     */
    public double getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(double p) {
        this.p = p;
        reset();
    }

    /**
     * @return the gamma
     */
    public double getGamma() {
        return gamma;
    }

    /**
     * @param gamma the gamma to set
     */
    public void setGamma(double gamma) {
        this.gamma = gamma;
        reset();
    }

    /**
     * @return the theta
     */
    public double getTheta() {
        return theta;
    }

    /**
     * @param theta the theta to set
     */
    public void setTheta(double theta) {
        this.theta = theta;
        reset();
    }

    /**
     * @return the current_pi
     */
    public Politique getCurrent_pi() {
        return current_pi;
    }

    /**
     * @param current_pi the current_pi to set
     */
    public void setCurrent_pi(Politique current_pi) {
        this.current_pi = current_pi;
    }

    /**
     * @return the current_V_from_evaluation
     */
    public double[] getCurrent_V_from_evaluation() {
        return current_V_from_evaluation;
    }

    /**
     * @param current_V_from_evaluation the current_V_from_evaluation to set
     */
    public void setCurrent_V_from_evaluation(double[] current_V_from_evaluation) {
        this.current_V_from_evaluation = current_V_from_evaluation;
    }

    /**
     * @return the current_V_from_value_iteration
     */
    public double[] getCurrent_V_from_value_iteration() {
        return current_V_from_value_iteration;
    }

    /**
     * @param current_V_from_value_iteration the current_V_from_value_iteration
     * to set
     */
    public void setCurrent_V_from_value_iteration(double[] current_V_from_value_iteration) {
        this.current_V_from_value_iteration = current_V_from_value_iteration;
    }
}
